package com.java11.java11.features;
class sample {
	String str= null;
}

public class StringNewAPI {

	public static void main(String[] args) {
//UseCase1 isBlank()
		//java11
		//Returns if the string is empty or contains only white space
		System.out.println(" ".isBlank());
//UseCase2: strip()
		//java11
		// Returns a string whose value is this string, with all leading and trailing
		// white space removed.
		System.out.println(" LR ".strip());
//Usecase3: stripLeading()
		//java11
		// Returns a string whose value is this string, with all leading white space
		//removed.
		System.out.println(" LR ".stripLeading());
// Usecase4:stripTrailing()
		//java11
		// a string whose value is this string, with all trailing white space
		System.out.println(" LR ".stripTrailing());
// Usecase5:lines()
		//java11
		// Returns a stream of lines extracted from this string, separated by line
		"Line\nLine1\nLine2\nLine3\nLine4\nLine5".lines().forEach(System.out::println);

	}

}
