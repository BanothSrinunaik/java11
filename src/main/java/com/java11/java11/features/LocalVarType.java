package com.java11.java11.features;

public class LocalVarType {

	public static <Var> void main(String[] args) {
		int userid = 123456;  // sureid => int
		
		 FuncInter obj = (var a, var b) -> a+b;
         System.out.println(obj.opertaion(20, 20));
	}

}
interface FuncInter 
{
	int opertaion(int a, int b);
	
}