package com.java11.java11.features;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.el.stream.Stream;

public class StringEnhancements { 
	
	public static void main(String[] arg) {
		String a="srinu\n"
					+"ram\n"
					+"abcd\n"
					+ "nanduhjhjjhhj\n" 
					+"raj\n";
		//Strip vs trim
		a.lines().forEach(System.out::println);
		Stream st = (Stream) a.lines();
		@SuppressWarnings("unchecked")
		List<String> list= ((java.util.stream.Stream<String>) st).collect(Collectors.toList());
		System.out.println(list);
		
	}
}
